package csql

import (
	"P002/config"
	"P002/models"
	"context"
	"database/sql"
	"errors"
	"fmt"
	"log"
	"time"
)

const (
	table          = "pelajar"
	layoutDateTime = "2006-01-02 15:04:05"
)

//var Rcheck int
var Rcheck int
var Jcheck int

//var RedHand = Rcheck

// GetAll
func GetAll(ctx context.Context) ([]models.Pelajar, error) {

	var pelajars []models.Pelajar // var pelajar merupakkan struct pelajar yang berada di models

	db, err := config.MySQL()

	if err != nil {
		log.Fatal("Cant connect to MySQL", err)
	}

	queryText := fmt.Sprintf("SELECT * FROM %v Order By id DESC", table)

	rowQuery, err := db.QueryContext(ctx, queryText)

	if err != nil {
		log.Fatal(err)
	}

	for rowQuery.Next() {
		var pelajar models.Pelajar
		var createdAt, updatedAt string

		if err = rowQuery.Scan(
			&pelajar.ID,
			&pelajar.NomorInduk,
			&pelajar.Nama,
			&pelajar.Jurusan,
			&createdAt,
			&updatedAt); err != nil {
			return nil, err
		}

		//  Change format string to datetime for created_at and updated_at
		pelajar.CreatedAt, err = time.Parse(layoutDateTime, createdAt)

		if err != nil {
			log.Fatal(err)
		}

		pelajar.UpdatedAt, err = time.Parse(layoutDateTime, updatedAt)

		if err != nil {
			log.Fatal(err)
		}

		pelajars = append(pelajars, pelajar)
	}

	return pelajars, nil
}

//Insert
func Insert(ctx context.Context, pel models.Pelajar) error {
	db, err := config.MySQL()

	if err != nil {
		log.Fatal("Can't connect to MySQL", err)
	}

	queryText := fmt.Sprintf("INSERT INTO %v (nomor_induk, nama, jurusan, created_at, updated_at) values(%v,'%v','%v','%v','%v')",
		table,
		pel.NomorInduk,
		pel.Nama,
		pel.Jurusan,
		time.Now().Format(layoutDateTime),
		time.Now().Format(layoutDateTime))

	_, err = db.ExecContext(ctx, queryText)

	if err != nil {
		return err
	}
	return nil
}

// Update
func Update(ctx context.Context, pel models.Pelajar) error {

	db, err := config.MySQL()

	if err != nil {
		log.Fatal("Can't connect to MySQL", err)
	}

	queryText := fmt.Sprintf("UPDATE %v set nomor_induk = %d, nama ='%s', jurusan = '%s', updated_at = '%v' where id = '%d'",
		table,
		pel.NomorInduk,
		pel.Nama,
		pel.Jurusan,
		time.Now().Format(layoutDateTime),
		pel.ID,
	)
	fmt.Println(queryText)

	_, err = db.ExecContext(ctx, queryText)

	if err != nil {
		return err
	}

	return nil
}

// Delete
func Delete(ctx context.Context, pel models.Pelajar) error {

	db, err := config.MySQL()

	if err != nil {
		log.Fatal("Can't connect to MySQL", err)
	}

	queryText := fmt.Sprintf("DELETE FROM %v where id = '%d'", table, pel.ID)

	s, err := db.ExecContext(ctx, queryText)

	if err != nil && err != sql.ErrNoRows {
		return err
	}

	check, err := s.RowsAffected()
	fmt.Println(check)
	if check == 0 {
		return errors.New("id tidak ada")
	}

	return nil
}

//func GetSome(ctx context.Context, jurusan string) ([]models.Pelajar, error)
func GetSome(ctx context.Context, jurusan string) ([]models.Pelajar, error) {

	var pelajars []models.Pelajar // var pelajar merupakkan struct pelajar yang berada di models

	db, err := config.MySQL()

	if err != nil {
		log.Fatal("Cant connect to MySQL", err)
	}

	queryText := fmt.Sprintf("SELECT * FROM %v WHERE jurusan like '%v' Order By id DESC", table, "%"+jurusan+"%")

	rowQuery, err := db.QueryContext(ctx, queryText)

	if err != nil {
		log.Fatal(err)
	}

	Jcheck = 0

	for rowQuery.Next() {
		var pelajar models.Pelajar
		var createdAt, updatedAt string

		if err = rowQuery.Scan(
			&pelajar.ID,
			&pelajar.NomorInduk,
			&pelajar.Nama,
			&pelajar.Jurusan,
			&createdAt,
			&updatedAt); err != nil {
			return nil, err
		}

		//  Change format string to datetime for created_at and updated_at
		pelajar.CreatedAt, err = time.Parse(layoutDateTime, createdAt)

		if err != nil {
			log.Fatal(err)
		}

		pelajar.UpdatedAt, err = time.Parse(layoutDateTime, updatedAt)

		if err != nil {
			log.Fatal(err)
		}

		pelajars = append(pelajars, pelajar)

		Jcheck++
	}

	fmt.Println(Jcheck)

	return pelajars, nil
}

func GetPelajarByName(ctx context.Context, nama string) ([]models.Pelajar, error) {

	var pelajars []models.Pelajar // var pelajar merupakkan struct pelajar yang berada di models

	db, err := config.MySQL()

	if err != nil {
		log.Fatal("Cant connect to MySQL", err)
	}

	queryText := fmt.Sprintf("SELECT * FROM %v WHERE nama like '%v' Order By id DESC", table, "%"+nama+"%")

	rowQuery, err := db.QueryContext(ctx, queryText)

	if err != nil {
		log.Fatal(err)
	}

	for rowQuery.Next() {
		var pelajar models.Pelajar
		var createdAt, updatedAt string

		if err = rowQuery.Scan(
			&pelajar.ID,
			&pelajar.NomorInduk,
			&pelajar.Nama,
			&pelajar.Jurusan,
			&createdAt,
			&updatedAt); err != nil {
			return nil, err
		}

		//  Change format string to datetime for created_at and updated_at
		pelajar.CreatedAt, err = time.Parse(layoutDateTime, createdAt)

		if err != nil {
			log.Fatal(err)
		}

		pelajar.UpdatedAt, err = time.Parse(layoutDateTime, updatedAt)

		if err != nil {
			log.Fatal(err)
		}

		pelajars = append(pelajars, pelajar)

		Rcheck++
	}

	fmt.Println(Rcheck)

	return pelajars, nil
}

func DeleteX(ctx context.Context, pel models.Pelajar) error {

	db, err := config.MySQL()

	if err != nil {
		log.Fatal("Can't connect to MySQL", err)
	}

	queryText := fmt.Sprintf("DELETE FROM %v where id = '%d'", table, pel.ID)

	s, err := db.ExecContext(ctx, queryText)

	if err != nil && err != sql.ErrNoRows {
		return err
	}

	check, err := s.RowsAffected()
	fmt.Println(check)
	if check == 0 {
		return errors.New("ZZ Cannon")
	}

	return nil
}

//func DX(r1 int) int {
//	r1 = Rcheck
//	return r1
//}
