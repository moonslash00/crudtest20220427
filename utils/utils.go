package utils

import (
	"encoding/json"
	"net/http"
)

func ResponseJSON(w http.ResponseWriter, p interface{}, status int) {
	ubahkeByte, err := json.Marshal(p)

	w.Header().Set("Content-Type", "application/json")

	if err != nil {
		http.Error(w, "error", http.StatusBadRequest)
	}
	w.Header().Set("Content-Type", "application/json") // Saat ditulis di Header
	w.WriteHeader(status)                              // Menuliskan status
	w.Write([]byte(ubahkeByte))                        // Menerapkan json Marshall
}
