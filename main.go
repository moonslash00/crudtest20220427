package main

import (
	"P002/config"
	"P002/csql"
	"P002/models"
	"P002/utils"
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"time"
)

const (
	table          = "pelajar"
	layoutDateTime = "2006-01-02 15:04:05"
)

var Rcheck2 int
var struk models.Error

func main() {
	fmt.Println("Proses Sedang Berjalan")

	http.HandleFunc("/pelajar", GetPelajar)
	http.HandleFunc("/pelajar/create", PostPelajar)
	http.HandleFunc("/pelajar/update", UpdatePelajar)
	http.HandleFunc("/pelajar/delete", DeletePelajar)
	http.HandleFunc("/pelajar/search", SearchPelajar)
	http.HandleFunc("/pelajar/name", SearchName)
	http.HandleFunc("/pelajar/cdel", CopyDeletePelajar)
	http.HandleFunc("/pelajar/nx", SearchNameX)
	//http.HandleFunc("/pelajar/jj", GetJ)
	err := http.ListenAndServe(":7020", nil)

	if err != nil {
		log.Fatal(err)
	}
}

// GET
func GetPelajar(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		ctx, cancel := context.WithCancel(context.Background())

		defer cancel()

		pelajars, err := csql.GetAll(ctx)

		if err != nil {
			fmt.Println(err)
		}

		utils.ResponseJSON(w, pelajars, http.StatusOK)
		return
	}
	struk.Error = "Tidak di izinkan"
	//http.Error(w, "Tidak di izinkan", http.StatusNotFound)
	utils.ResponseJSON(w, struk, http.StatusMethodNotAllowed)
	return
}

// POST
func PostPelajar(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {

		if r.Header.Get("Content-Type") != "application/json" {
			http.Error(w, "Gunakan content type application / json", http.StatusBadRequest)
			return
		}

		ctx, cancel := context.WithCancel(context.Background())
		defer cancel()

		var pel models.Pelajar

		if err := json.NewDecoder(r.Body).Decode(&pel); err != nil {
			utils.ResponseJSON(w, err, http.StatusBadRequest)
			return
		}

		if err := csql.Insert(ctx, pel); err != nil {
			utils.ResponseJSON(w, err, http.StatusInternalServerError)
			return
		}

		res := map[string]string{
			"status": "Succesfully",
		}

		utils.ResponseJSON(w, res, http.StatusCreated)
		return
	}
	struk.Error = "Tidak di izinkan"
	//http.Error(w, "Tidak di izinkan", http.StatusMethodNotAllowed)
	utils.ResponseJSON(w, struk, http.StatusMethodNotAllowed)
	return
}

// Update
func UpdatePelajar(w http.ResponseWriter, r *http.Request) {
	if r.Method == "PUT" {

		if r.Header.Get("Content-Type") != "application/json" {
			//struk.Error = "Gunakan content type application / json"
			//utils.ResponseJSON(w, struk, http.StatusBadRequest)
			http.Error(w, "Gunakan content type application / json", http.StatusBadRequest)
			return
		}

		ctx, cancel := context.WithCancel(context.Background())
		defer cancel()

		var pel models.Pelajar

		if err := json.NewDecoder(r.Body).Decode(&pel); err != nil {
			utils.ResponseJSON(w, err, http.StatusBadRequest)
			return
		}

		fmt.Println(pel)

		if err := csql.Update(ctx, pel); err != nil {
			utils.ResponseJSON(w, err, http.StatusInternalServerError)
			return
		}

		res := map[string]string{
			"status": "Succesfully",
		}

		utils.ResponseJSON(w, res, http.StatusCreated)
		return
	}
	struk.Error = "Tidak di izinkan"
	//http.Error(w, "Tidak di izinkan", http.StatusMethodNotAllowed)
	utils.ResponseJSON(w, struk, http.StatusMethodNotAllowed)
	return
}

// Delete
func DeletePelajar(w http.ResponseWriter, r *http.Request) {

	if r.Method == "DELETE" {

		ctx, cancel := context.WithCancel(context.Background())
		defer cancel()

		var pel models.Pelajar

		id := r.URL.Query().Get("id")

		struk.Error = "Tidak Boleh Kosong"

		if id == "" {
			utils.ResponseJSON(w, struk, http.StatusBadRequest)
			return
		}
		pel.ID, _ = strconv.Atoi(id)

		if err := csql.Delete(ctx, pel); err != nil {

			kesalahan := map[string]string{
				"error": fmt.Sprintf("%v", err),
			}

			utils.ResponseJSON(w, kesalahan, http.StatusInternalServerError)
			return
		}

		res := map[string]string{
			"status": "Succesfully",
		}

		utils.ResponseJSON(w, res, http.StatusOK)
		return
	}
	//var testo string
	struk.Error = "Tidak di izinkan"
	//http.Error(w, "Tidak di izinkan", http.StatusMethodNotAllowed)
	utils.ResponseJSON(w, struk, http.StatusMethodNotAllowed)
	return
}

/*
func SearchPelajar(w http.ResponseWriter, r *http.Request) {

	if r.Method == "GET" {

		ctx, cancel := context.WithCancel(context.Background())
		defer cancel()

		var pel models.Pelajar

		id := r.URL.Query().Get("id")

		if id == "" {
			utils.ResponseJSON(w, "id tidak boleh kosong", http.StatusBadRequest)
			return
		}
		pel.ID, _ = strconv.Atoi(id)

		if err := csql.Delete(ctx, pel); err != nil {

			kesalahan := map[string]string{
				"error": fmt.Sprintf("%v", err),
			}

			utils.ResponseJSON(w, kesalahan, http.StatusInternalServerError)
			return
		}

		res := map[string]string{
			"status": "Succesfully",
		}

		utils.ResponseJSON(w, res, http.StatusOK)
		return
	}

	http.Error(w, "Tidak di izinkan", http.StatusMethodNotAllowed)
	return
}
*/

func SearchPelajar(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {

		ctx, cancel := context.WithCancel(context.Background())

		defer cancel()

		//var pel models.Pelajar

		jurusan := r.URL.Query().Get("jurusan")
		//var Jcheck int
		//csql.Rcheck = Jcheck

		if jurusan == "" {
			struk.Error = "Tidak Boleh Kosong"
			utils.ResponseJSON(w, struk, http.StatusBadRequest)
			return
		}

		//jurusan := "Tek"

		pelajars, err := csql.GetSome(ctx, jurusan)

		//pelajars, err := csql.GetSome(ctx)
		if csql.Jcheck == 0 {
			struk.Error = "No Result"
			utils.ResponseJSON(w, struk, http.StatusNotFound)
			return
		}

		if err != nil {
			fmt.Println(err)
		}

		utils.ResponseJSON(w, pelajars, http.StatusOK)
		return
	}
	struk.Error = "Tidak di izinkan"
	//http.Error(w, "Tidak di izinkan", http.StatusMethodNotAllowed)
	utils.ResponseJSON(w, struk, http.StatusMethodNotAllowed)
	return
}

func SearchName(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {

		//if r.Header.Get("Content-Type") != "application/json" {
		//	http.Error(w, "Gunakan content type application / json", http.StatusBadRequest)
		//	return
		//}

		ctx, cancel := context.WithCancel(context.Background())

		defer cancel()

		//var pel models.Pelajar

		nama := r.URL.Query().Get("nama")

		//	pelajar.CreatedAt, err = time.Parse(layoutDateTime, createdAt)
		//var pelajars []models.Error
		//var struk models.Error
		//var createdAt, updatedAt string
		struk.Error = "Tidak Boleh Kosong"
		//var pelajars []models.Pelajar
		//var KLA int
		//KLA = csql.Rcheck
		var Rcheck int
		csql.Rcheck = Rcheck
		//var RedHand int
		//csql.RedHand = RedHand
		//nilrespons := map[string]string{
		//	"status": "Tidak boleh kosong",
		//}

		if nama == "" {
			utils.ResponseJSON(w, struk, http.StatusBadRequest)
			return
		}

		//fmt.Println(Rcheck)
		//fmt.Println(KLA)

		pelajars, err := csql.GetPelajarByName(ctx, nama)

		if csql.Rcheck == 0 {
			struk.Error = "No Result"
			utils.ResponseJSON(w, struk, http.StatusNotFound)
			return
		}
		//Rcheck++
		//fmt.Println(Rcheck)

		//fmt.Println(KLA)

		//if Rcheck == 0 {
		//	struk.Error = "Down"
		//	utils.ResponseJSON(w, struk, http.StatusBadRequest)
		//	return
		//}

		//fmt.Println(KLA)

		//if KLA == 0 {
		//	fmt.Println(KLA)
		//errors.New("tak ditemukkan")
		//}
		//fmt.Println(KLA)

		if err != nil {
			//fmt.Println(err)
			if err == sql.ErrNoRows {
				kesalahan := map[string]string{
					"error": fmt.Sprintf("%v", err),
				}

				utils.ResponseJSON(w, kesalahan, http.StatusInternalServerError)
				return
			}
		}

		utils.ResponseJSON(w, pelajars, http.StatusOK)
		return
	}
	struk.Error = "Tidak di izinkan"
	//http.Error(w, "Tidak di izinkan", http.StatusMethodNotAllowed)
	utils.ResponseJSON(w, struk, http.StatusMethodNotAllowed)
	return
}

/*
func GetJ(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		ctx, cancel := context.WithCancel(context.Background())

		jurusan := r.URL.Query().Get("jurusan")

		defer cancel()

		pelajars, err := csql.GetD(jurusan, ctx)

		if err != nil {
			fmt.Println(err)
		}

		utils.ResponseJSON(w, pelajars, http.StatusOK)
		return
	}

	http.Error(w, "Tidak di izinkan", http.StatusNotFound)
	return
}
*/

func CopyDeletePelajar(w http.ResponseWriter, r *http.Request) {

	if r.Method == "DELETE" {

		ctx, cancel := context.WithCancel(context.Background())
		defer cancel()

		var pel models.Pelajar

		id := r.URL.Query().Get("id")

		if id == "" {
			utils.ResponseJSON(w, "id tidak boleh kosong", http.StatusBadRequest)
			return
		}
		pel.ID, _ = strconv.Atoi(id)

		if err := csql.DeleteX(ctx, pel); err != nil {

			kesalahan := map[string]string{
				"error": fmt.Sprintf("%v", err),
			}

			utils.ResponseJSON(w, kesalahan, http.StatusInternalServerError)
			return
		}

		res := map[string]string{
			"status": "Succesfully",
		}

		utils.ResponseJSON(w, res, http.StatusOK)
		return
	}

	http.Error(w, "Tidak di izinkan", http.StatusMethodNotAllowed)
	return
}

func GPBN(ctx context.Context, nama string) ([]models.Pelajar, error) {

	var pelajars []models.Pelajar // var pelajar merupakkan struct pelajar yang berada di models

	db, err := config.MySQL()

	if err != nil {
		log.Fatal("Cant connect to MySQL", err)
	}

	queryText := fmt.Sprintf("SELECT * FROM %v WHERE nama like '%v' Order By id DESC", table, "%"+nama+"%")

	rowQuery, err := db.QueryContext(ctx, queryText)

	if err != nil {
		log.Fatal(err)
	}

	Rcheck2 = 0

	for rowQuery.Next() {
		var pelajar models.Pelajar
		var createdAt, updatedAt string

		if err = rowQuery.Scan(
			&pelajar.ID,
			&pelajar.NomorInduk,
			&pelajar.Nama,
			&pelajar.Jurusan,
			&createdAt,
			&updatedAt); err != nil {
			return nil, err
		}

		//  Change format string to datetime for created_at and updated_at
		pelajar.CreatedAt, err = time.Parse(layoutDateTime, createdAt)

		if err != nil {
			log.Fatal(err)
		}

		pelajar.UpdatedAt, err = time.Parse(layoutDateTime, updatedAt)

		if err != nil {
			log.Fatal(err)
		}

		pelajars = append(pelajars, pelajar)

		Rcheck2++
	}

	fmt.Println(Rcheck2)

	if Rcheck2 == 0 {
		//fmt.Println(Rcheck)
		//errors.New("tak ditemukkan")
	}

	//fmt.Println(check)

	//if check == 0 {
	//	return pelajars, errors.New("tak ditemukkan")
	//}

	return pelajars, nil
}

func SearchNameX(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {

		//if r.Header.Get("Content-Type") != "application/json" {
		//	http.Error(w, "Gunakan content type application / json", http.StatusBadRequest)
		//	return
		//}

		ctx, cancel := context.WithCancel(context.Background())

		defer cancel()

		//var pel models.Pelajar

		nama := r.URL.Query().Get("nama")

		//	pelajar.CreatedAt, err = time.Parse(layoutDateTime, createdAt)
		//var pelajars []models.Error
		var struk models.Error
		//var createdAt, updatedAt string
		struk.Error = "Tidak Boleh Kosong"
		//var pelajars []models.Pelajar

		//nilrespons := map[string]string{
		//	"status": "Tidak boleh kosong",
		//}

		if nama == "" {
			utils.ResponseJSON(w, struk, http.StatusBadRequest)
			return
		}

		pelajars, err := GPBN(ctx, nama)

		if Rcheck2 == 0 {
			struk.Error = "Down"
			utils.ResponseJSON(w, struk, http.StatusBadRequest)
			return
		}

		if err != nil {
			//fmt.Println(err)
			if err == sql.ErrNoRows {
				kesalahan := map[string]string{
					"error": fmt.Sprintf("%v", err),
				}

				utils.ResponseJSON(w, kesalahan, http.StatusInternalServerError)
				return
			}
		}

		utils.ResponseJSON(w, pelajars, http.StatusOK)
		return
	}

	http.Error(w, "Tidak di izinkan", http.StatusNotFound)
	return
}
