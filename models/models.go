package models

import (
	"time"
)

type (
	Pelajar struct {
		ID         int       `json:"id"`          /*ID ditulis di JSON id*/
		NomorInduk int       `json:"nomor_induk"` /*Nomor Induk ditulis di JSON nomor_induk*/
		Nama       string    `name:"nama"`        /*Nama  ditulis di JSON nama*/
		Jurusan    string    `json:"jurusan"`     /*Jurusan ditulis di JSON jurusan*/
		CreatedAt  time.Time `json:"created_at"`  /*CreatedAt ditulis di JSON created_at*/
		UpdatedAt  time.Time `json:"updated_at"`  /*UpdatedAt ditulis di JSON updated_at*/
	}
)

type (
	Error struct {
		Error string `json:"error"`
	}
)

/*
type (
	Pelajar struct {
		ID         int       `json:"id"`
		NomorInduk int       `json:"nomor_induk"`
		Nama       string    `name:"nama"`
		Jurusan    string    `json:"jurusan"`
		CreatedAt  time.Time `json:"created_at"`
		UpdatedAt  time.Time `json:"updated_at"`
	}
)
*/
