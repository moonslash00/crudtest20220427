package config

import (
	"database/sql"
	"fmt"

	_ "github.com/go-sql-driver/mysql"
)

const (
	username string = "root"
	password string = "MySQL1-10"
	database string = "testdb001"
)

// konstanta berupa username,password, dan database

var (
	dsn = fmt.Sprintf("%v:%v@/%v", username, password, database) // var dsn untuk memanggil username,password,database yang telah dideklarasikan di konstanta
)

// Fungsi untuk membuka mysql
func MySQL() (*sql.DB, error) {
	db, err := sql.Open("mysql", dsn)

	if err != nil {
		return nil, err
	}

	return db, nil
}
