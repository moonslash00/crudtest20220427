-- MySQL dump 10.13  Distrib 8.0.28, for Win64 (x86_64)
--
-- Host: localhost    Database: testdb001
-- ------------------------------------------------------
-- Server version	8.0.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `pelajar`
--

DROP TABLE IF EXISTS `pelajar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pelajar` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nomor_induk` bigint NOT NULL,
  `nama` varchar(100) NOT NULL,
  `jurusan` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci PACK_KEYS=0;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pelajar`
--

LOCK TABLES `pelajar` WRITE;
/*!40000 ALTER TABLE `pelajar` DISABLE KEYS */;
INSERT INTO `pelajar` VALUES (1,2213040001,'Adelia Putri Sanjaya','Teknik Sipil','2022-04-27 03:27:43','2022-04-27 03:38:45'),(2,2213040003,'Bertram Adi Putro','Teknik Industri','2022-04-27 03:27:43','2022-04-27 03:38:45'),(3,2213040003,'Claudia Wilson','Ilmu Kehutanan','2022-04-14 01:03:17','2022-04-14 02:06:52'),(4,2213040004,'Dea Ananda Hartati','Teknik Sipil','2022-04-14 02:07:47','2022-04-14 02:07:47'),(5,2213040005,'Sari Nahsimin','Psikologi','2022-04-19 21:18:01','2022-04-19 21:18:01'),(6,2213040006,'Fania Husni Utami','Ilmu Kelautan','2022-04-19 21:22:01','2022-04-19 21:22:01'),(7,2213040007,'Galih Nur Fathoni','Teknik Mesin','2022-04-19 21:22:05','2022-04-19 21:22:05'),(8,2213040008,'Nani Wiratmojo','Sosiologi','2022-04-19 11:22:11','2022-04-19 11:22:11'),(9,2213040009,'Nunung Sarwanah','Sosiologi','2022-04-19 11:31:01','2022-04-19 11:31:01'),(10,2213040100,'Ethan Skorr','Ekonomi','2022-04-25 21:16:21','2022-04-25 21:16:21');
/*!40000 ALTER TABLE `pelajar` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-04-25 22:52:12
